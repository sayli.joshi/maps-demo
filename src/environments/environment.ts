// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey:"AIzaSyBgP9PiTd5C_WFEJjoL83I6x8wujHfkC1E",
  apiBaseUrl: 'https://data.geoiq.io/dataapis/v1.0/covid',
  authKey:'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODgwNjU0MDIsIm5iZiI6MTU4ODA2NTQwMiwianRpIjoiYTMxY2ZkZDUtMGYwNi00MTJlLThkMmQtYzMxMDMzNDA3OTEzIiwiZXhwIjoxNjc0NDY1NDAyLCJpZGVudGl0eSI6IjQ1ZTgyMjQzLTgzMDYtNGZiNi1hNzE1LTgzZWJjMDZjOTcxZiIsImZyZXNoIjp0cnVlLCJ0eXBlIjoiYWNjZXNzIn0.9HDMpKY3xjXVU7aoSEsglAxJ4mgKqCpoAmv4nQPmC3Q'
 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
