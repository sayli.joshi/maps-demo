import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapViewComponent } from './map-view/map-view.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { AgmDirectionModule } from 'agm-direction';   // agm-direction
import { MapViewService } from './map-view/map-view.service';

@NgModule({
  declarations: [
    AppComponent,
    MapViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiKey,
      libraries: ['geometry','places']
    }),
    AgmDirectionModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgmJsMarkerClustererModule
  ],
  providers: [MapViewService],
  bootstrap: [AppComponent]
})
export class AppModule { }
