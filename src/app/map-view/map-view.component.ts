
import { Component, ElementRef, NgZone, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormControl } from "@angular/forms";
import { MapsAPILoader } from '@agm/core';
import { Constants } from 'src/app/config/constants';
import * as _ from 'lodash';
import { MapViewService } from './map-view.service';
import { RouteReuseStrategy } from '@angular/router';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.css']
})
export class MapViewComponent implements OnInit, AfterViewInit {
  map: any;
  bounds: any;
  mapProperties = Constants.MAP_CONFIG;
  previousIW = null;
  currentIW = null;
  openedWindow = 0;
  surroundCoord: any = [];
  geocoder: any;
  markers = [];
  centerChanged = false;
  nearbyMarkers = [];
  public searchControl: FormControl;
  origin: string = 'Koramangala, Bengaluru, Karnataka, India'
  destination: string = 'Vyalikaval Police Station, Vyalikaval, Guttahalli, Bengaluru, Karnataka, India'
  waypoints = []
  public renderOptions = {
    draggable: true,
}
showZones=false;
  showDirection = false;
  @ViewChild("search", { static: true }) searchElementRef;
  @ViewChild("orig", { static: true }) orig;
  @ViewChild("dest", { static: true }) dest;

  constructor(private mapsAPILoader: MapsAPILoader,private cdRef:ChangeDetectorRef,
    private ngZone: NgZone, private MapViewService: MapViewService) { }

  ngOnInit() {
    this.setData();
    this.setOrig();
    this.setDest();

  }
  ngAfterViewInit() {

  }
  onReady($event) {
    this.map = $event;
  }
  toggleLayer(flag){
    console.log("flag     ",flag)
    if(flag){
      this.showZones=true;
      this.map.data.loadGeoJson('./../../assets/cities.geojson');
      this.map.data.setStyle({
        // icon: '//example.com/path/to/image.png',
        fillColor: 'red',
        fillOpacity: 0.3,
        strokeColor: 'red',
        strokeOpacity: 1,
        strokeWeight: 2
      });
    }else{
      this.showZones=false;
      this.map.data.forEach((feature) =>{
        this.map.data.remove(feature);
    });
    }

  }
  markerClick(infoWindow) {
    if (this.previousIW) {
      this.currentIW = infoWindow;
      this.previousIW.close();
    }
    this.previousIW = infoWindow;
    this.openedWindow = 0;
  }
  isInfoWindowOpen(markerData) {
    return this.openedWindow == markerData.id;
  }
  setData() {
    //create search FormControl
    this.searchControl = new FormControl();
    //set current position
    // this.setCurrentPosition();
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.geocoder = new google.maps.Geocoder();
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        // types: ["address"],
        componentRestrictions: { country: ["IN"] }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.mapProperties.lat = place.geometry.location.lat();
          this.mapProperties.lng = place.geometry.location.lng();
          this.mapProperties.zoom = 12;
          this.getAllCoords()
        });
      });
    });
  }
  setOrig() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.orig.nativeElement, {
        // types: ["address","geocode"],
        componentRestrictions: { country: ["IN"] }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.origin = place.formatted_address;
          this.refreshDirection()
        });
      });
    });
  }
  setDest() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.dest.nativeElement, {
        // types: ["address","geocode"],
        componentRestrictions: { country: ["IN"] }
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.destination = place.formatted_address;
          this.refreshDirection()
        });
      });
    });
  }
  toggleDirection(){
    this.showDirection=!this.showDirection
    console.log(this.showDirection)
  }
  getStatus() {
    this.markers = [];
    let coordArray = [];
    this.nearbyMarkers = [];
    this.surroundCoord.forEach((ele) => {
      coordArray.push(ele.latlng)
    })
    this.MapViewService.getData(coordArray).subscribe(
      (response: any) => {
        if (response.status = 200) {
          let data = response.data
          for (let i = 0; i < this.surroundCoord.length; i++) {
            let icon;
            let desc;
            if(data[i].inContainmentZone){
              icon = "/assets/icon/darkBlue.png"
              desc=data[i].district
                }
            else{
              icon = "/assets/icon/green.png"
              desc="Not a containment zone"
            }
              // switch (data[i].districtZoneType) {
              //   case 'Red':
              //     icon = data[i].inContainmentZone?"/assets/icon/darkBlue.png":"/assets/icon/red.png"
              //     desc = data[i].inContainmentZone?'Containment Zone':'Red Zone'
              //     break;
              //   case 'Containment':
              //     icon = "/assets/icon/darkBlue.png"
              //     desc = 'Containment Zone'
              //     break;
              //   case 'Green':
              //     icon = "/assets/icon/green.png"
              //     desc = 'Green Zone'
              //     break;
              //   case 'Yellow':
              //     icon = "/assets/icon/yellow.png"
              //     desc = 'Yellow Zone'
              //     break;
              //   case 'Orange':
              //     icon = "/assets/icon/orange.png"
              //     desc = 'Orange Zone'
              //     break;
              //   default:
              //     icon = "/assets/icon/black.png"
              //     desc = data[i].availability ? 'No zone has been declared yet' : 'No data present for this region'
  
              // }
       
         
            this.markers.push({
              ...this.surroundCoord[i],
              ...{
                availability: data[i].containmentsAvailability,
                address: data[i].district,
                name: data[i].containmentZoneName,
                inContainmentZone:data[i].inContainmentZone,
                iconUrl: icon,
                desc: desc,
                showNearby: false
              }
            })

          }
          if (this.markers.length == this.surroundCoord.length) {
            this.markers.forEach((ele) => {
              if (ele.address == null && ele.iconUrl == "/assets/icon/black.png") {
                let latlng = new google.maps.LatLng(ele.lat, ele.lng);
                let request = {
                  location: latlng
                };
                // this.geocoder.geocode(request, function (results, status) {
                //   if (status == google.maps.GeocoderStatus.OK) {
                //     if (results[1]) {
                //       ele.address = results[1].formatted_address
                //     } else {
                //       console.log('Location not found');
                //     }
                //   } else {
                //     console.log('Geocoder failed due to: ' + status);
                //   }
                // });
              }
              if (ele.iconUrl == "/assets/icon/black.png" || ele.iconUrl == "/assets/icon/green.png") {
                ele.showNearby = true
              }
            })
          }
        }
        else {
          alert("Unable to get data")
        }
      }, (error) => {
        console.log("Error in component: ", error)
      })

  }
  getAllCoords() {
    this.surroundCoord = [];
    this.nearbyMarkers = [];
    let bearing = [45, 90, 135, 180, 225, 270, 315, 360];
    let distance = 2000
    let pointA = new google.maps.LatLng(this.mapProperties.lat, this.mapProperties.lng);
    for (let i = 0; i < bearing.length; i++) {
      let pointB = google.maps.geometry.spherical.computeOffset(pointA, this.mapProperties.radius, bearing[i]);
      this.surroundCoord.push({ lat: pointB.lat(), lng: pointB.lng(), bearing: bearing[i], latlng: [pointB.lat(), pointB.lng()] })
    }
    this.surroundCoord.push({ lat: this.mapProperties.lat, lng: this.mapProperties.lng, bearing: 0, latlng: [this.mapProperties.lat, this.mapProperties.lng] })
    this.getStatus();
  }
  showNearbyPlaces(data) {
    this.mapsAPILoader.load().then(() => {
      let service = new google.maps.places.PlacesService(this.map);
      let req = {
        location: { lat: data.lat, lng: data.lng },
        radius: 1000,
        types: ['restaurant']
      };
      let ref = this
      service.nearbySearch(req, function (results, status, pagination) {
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
          console.log("Failed due to : ", status);
          return;
        } else {
          ref.createMarker(results)
          // if (pagination.hasNextPage) {
          //   pagination.nextPage()
          // }
        }
      });

    });
  }
  createMarker(rawData) {
    this.nearbyMarkers = [];
    rawData.forEach((ele) => {
      let icon = {
        url: ele.icon, // url
        scaledSize: new google.maps.Size(32, 32), // scaled size
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
      };
      this.nearbyMarkers.push({
        name: ele.name,
        lat: ele.geometry.location.lat(),
        lng: ele.geometry.location.lng(),
        address: ele.vicinity,
        types: ele.types,
        icon: icon
      })
    })
  }
  backtoPoints() {
    this.nearbyMarkers = [];
  }
  addWaypoint(click) {
    this.waypoints.push({
      location: { lat: click.coords.lat, lng: click.coords.lng },
      stopover: false
    })
    this.refreshDirection()
    // this.cdRef.detectChanges()
  }
  refreshDirection(){
    this.showDirection = false
    setTimeout(()=>{
      this.showDirection = true
    },0)
  }
  // private setCurrentPosition() {
  //   if ("geolocation" in navigator) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       this.mapProperties.lat = position.coords.latitude;
  //       this.mapProperties.lng = 77.5946//position.coords.longitude;
  //       this.mapProperties.zoom = 12;
  //     });
  //   }

  // }
  getLabel(i){
    return (i+1).toString()
  }
  clearWaypoints(){
    this.waypoints=[];
  }
  clearMarkers(){
    this.markers=[];
  }
  getMapLink(){
    let waypointString='';
    let i=0;
    this.waypoints.forEach((ele)=>{
      waypointString=`${waypointString}${i==0?'':'|'}${ele.location.lat},${ele.location.lng}`
      i++;
    })
    return `https://www.google.com/maps/dir/?api=1&origin=${this.origin}&destination=${this.destination}&waypoints=${waypointString}`;
  }
}
