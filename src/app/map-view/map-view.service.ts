import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MapViewService {

  constructor(private http: HttpClient) { }

  
  getData(latlngsarray) :Observable<any>{
     let params = {'key': environment.authKey,'latlngs': latlngsarray}
    return this.http.post(`${environment.apiBaseUrl}/locationcheck`,params,{responseType:"text"}).pipe(
      map(response => {
        console.log(typeof response,response)
        if (response !== '') {
          return JSON.parse(response.replace(/\bNaN\b/g, "null"));
        } else {
          return {}
        }
      })
    );
   }

}
